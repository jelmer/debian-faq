<!-- retain these comments for translator revision tracking -->
<!-- Original version: $Revision: 1.29 $ -->
<chapt id="pkgtools">The Debian package management tools

<sect id="pkgprogs">What programs does Debian provide for managing its
  packages?

<p>There are multiple tools that are used to manage Debian packages,
from graphic or text-based interfaces to the low level tools used to
install packages. All the available tools rely on the lower level
tools to properly work and are presented here in decreasing complexity
level.

<p>It is important to understand that the higher level package
management tools such as <prgn/aptitude/ or <prgn/synaptic/ rely on
<prgn/apt/ which, itself, relies on <prgn/dpkg/ to manage the packages
in the system.

<p>See <url id="http://www.debian.org/doc/manuals/debian-reference/ch02.en.html" name="Chapter 2. Debian package management"> of the 
<url id="http://www.debian.org/doc/manuals/debian-reference/" 
name="Debian reference">
for more information about the Debian package management utilities.
This document is available in various languages and formats, see <url
id="http://www.debian.org/doc/user-manuals#quick-reference" name="the Debian
Reference entry in the DDP Users' Manuals overview">.

<!-- TODO: Describe tools to automatically build from sources Debian
packages including 'apt-build' or 'pbuilder' (in pkg_basics.sgml) -->

<sect1 id="dpkg">dpkg

<p>This is the main package management program. <prgn/dpkg/ can be
invoked with many options.  Some common uses are:
<list>
  <item>Find out all the options:  <tt>dpkg --help</tt>.
  <item>Print out the control file (and other information) for a specified
    package: <tt>dpkg --info foo_VVV-RRR.deb</tt>.
  <item>Install a package (including unpacking and configuring) onto the
    file system of the hard disk: <tt>dpkg --install foo_VVV-RRR.deb</tt>.
  <item>Unpack (but do not configure) a Debian archive into the file system
    of the hard disk: <tt>dpkg --unpack foo_VVV-RRR.deb</tt>.  Note that this
    operation does <em>not</em> necessarily leave the package in a usable
    state; some files may need further customization to run properly.
    This command removes any already-installed version of the program and
    runs the preinst (see <ref id="maintscripts">) script associated with
    the package.
  <item>Configure a package that already has been unpacked:
    <tt>dpkg --configure foo</tt>.  Among other things, this action runs the
    postinst (see <ref id="maintscripts">) script associated with the package.
    It also updates the files listed in the <tt>conffiles</tt> for this
    package. Notice that the 'configure' operation takes as its argument a
    package name (e.g., foo), <em/not/ the name of a Debian archive file
    (e.g., foo_VVV-RRR.deb).
  <item>Extract a single file named "blurf" (or a group of files
    named "blurf*") from a Debian archive:
    <tt>dpkg --fsys-tarfile foo_VVV-RRR.deb | tar -xf - 'blurf*'</tt>.
  <item>Remove a package (but not its configuration files):
    <tt>dpkg --remove foo</tt>.
  <item>Remove a package (including its configuration files):
    <tt>dpkg --purge foo</tt>.
  <item>List the installation status of packages containing the string
    (or regular expression) "foo*": <tt>dpkg --list 'foo*'</tt>.
</list>

<sect1 id="apt-get">APT

<p>APT is the <em>Advanced Package Tool</em>, an advanced interface to the
Debian packaging system which provides the <prgn/apt-get/ program.
It provides commandline tools for searching and managing packages, and
for querying information about them, as well as low-level access to all features
of the libapt-pkg library.  For more information, see the User's Guide in
<tt>/usr/share/doc/apt-doc/guide.html/index.html</tt> (you will have to install
the <package>apt-doc</package> package).

<p>Starting with Debian Jessie, some frequently used
<prgn/apt-get/ and <prgn/apt-cache/ commands have an equivalent via
the new <prgn/apt/ binary. This means some popular commands like
<prgn/apt-get update/, <prgn/apt-get install/, <prgn/apt-get remove/,
<prgn/apt-cache search/, or <prgn/apt-cache show/ now can also be called
simply via <prgn/apt/, say <prgn/apt update/, <prgn/apt install/,
<prgn/apt remove/, <prgn/apt search/, or <prgn/apt show/. 
The following is an overview of the old and new commands:

<example>
 apt-get update             ->  apt update
 apt-get upgrade            ->  apt upgrade
 apt-get dist-upgrade       ->  apt full-upgrade
 apt-get install package    ->  apt install package
 apt-get remove package     ->  apt remove package
 apt-get autoremove         ->  apt autoremove
 apt-cache search string    ->  apt search string
 apt-cache policy package   ->  apt list -a package
 apt-cache show package     ->  apt show package
 apt-cache showpkg package  ->  apt show -a package
</example>

<p>The <prgn/apt/ tool merges functionality of apt-get and apt-cache
and by default has a fancier colored output format, making it more
pleasant for humans. For usage in scripts or advanced use cases, 
apt-get is still preferable or needed.

<p><prgn/apt-get/ provides a simple way to
retrieve and install packages from multiple sources using the command
line.  Unlike <prgn/dpkg/, <prgn/apt-get/ does not understand .deb
files, it works with the packages proper name and can only install
.deb archives from a source specified in
<file>/etc/apt/sources.list</file>. <prgn/apt-get/ will call
<prgn/dpkg/ directly after downloading the .deb
archives<footnote>Notice that there are ports that make this tool
available with other package management systems, like Red Hat package
manager, also known as <prgn/rpm/</footnote> from the configured
sources.


<p>Some common ways to use <prgn/apt-get/ are:

<list>
       <item>To update the list of packages known by your system, you can run:
               <example>apt update</example>
       (you should execute this regularly to update your package lists)

       <item>To install the <var/foo/ package and all its dependencies, run:
               <example>apt install foo</example>

       <item>To remove the foo package from your system, run:
               <example>apt remove foo</example>

       <item>To remove the foo package and its configuration files from your
             system, run:
               <example>apt purge foo</example>

       <item>To list all packages for which newer versions are
             available, run:
               <example>apt list --upgradable</example>

       <item>To upgrade all the packages on your system (without installing
             extra packages or removing packages), run:
               <example>apt upgrade</example>

       <item>To upgrade all the packages on your system, and, if needed for a
             package upgrade, installing extra packages or removing packages, run:
               <example>apt full-upgrade</example>
             (The command <tt>upgrade</tt> keeps a package at its installed
             obsolete version if upgrading would need an extra package to be
             installed, for a new dependency to be satisfied. The
             <tt>full-upgrade</tt> command is less conservative.)

</list>

<p>Note that you must be logged in as root to perform any commands that
modify packages.

<p>Note that <prgn/apt-get/ now also installs recommended packages as
default, and thanks to its robustness it's the preferred program for package management from console to perform system
installation and major system upgrades.

<p>The apt tool suite also includes the <prgn/apt-cache/ tool to query
the package lists. You can use it to find packages providing specific
functionality through simple text or regular expression queries and
through queries of dependencies in the package management system.
Some common ways to use <prgn/apt-cache/ are:

<list>
       <item>To find packages whose description contain <var/word/:
               <example>apt search <var>word</var></example>

       <item>To print the detailed information of a package:
               <example>apt show <var>package</var></example>

       <item>To print the packages a given package depends on:
               <example>apt-cache depends <var>package</var></example>

       <item>To print detailed information on the versions available
               for a package and the packages that reverse-depends on
               it:
               <example>apt-cache showpkg <var>package</var></example>

</list>


<p>For more information, install the <package/apt/ package and read
<manref name="apt" section="8">,
<manref name="apt-get" section="8">, <manref name="sources.list" section="5">
and install the <package/apt-doc/ package and read
<file>/usr/share/doc/apt-doc/guide.html/index.html</file>.

<sect1 id="aptitude">aptitude

<p><prgn/aptitude/ is a package manager for &debian; systems that
provides a frontend to the apt package management
infrastructure. <prgn/aptitude/ is a text-based interface using the
curses library.  Actions may be performed from a visual interface or from the
command-line.

<p><prgn/aptitude/ can be used to perform management tasks in a
fast and easy way. It allows the user to view the list of packages and to
perform package management tasks such as installing, upgrading, and removing
packages.

<p><prgn/aptitude/ provides the functionality of
<prgn/apt-get/, as well as many additional features:

<list>
       <item><prgn/aptitude/ offers easy access to all versions of a package.

       <item><prgn/aptitude/ makes it easy to keep track of obsolete
       software by listing it under "Obsolete and Locally Created
       Packages".

       <item><prgn/aptitude/ includes a fairly powerful system for
       searching particular packages and limiting the package
       display. Users familiar with <prgn/mutt/ will pick up quickly,
       as <prgn/mutt/ was the inspiration for the expression syntax.

       <item><prgn/aptitude/ can be used to install the predefined tasks
       available. For more information see <ref id="tasksel">.

       <item><prgn/aptitude/ in full screen mode has <prgn/su/
       functionality embedded and can be run by a normal user. It will
       call <prgn/su/ (and ask for the root password, if any) when you
       really need administrative privileges.

</list>

<p>You can use <prgn/aptitude/ through a visual interface (simply run
<tt>aptitude</tt>) or directly from the command line. The command line
syntax used is very similar to the one used in <prgn/apt-get/. For
example, to install the <var>foo</var> package, you can run
<tt>aptitude install <var>foo</var></tt>.

<p>Note that <prgn/aptitude/ is the preferred program for daily package 
management from the console.

<p>For more information, read the manual page <manref
name="aptitude" section="8"> and install the <package/aptitude-doc/
package.

<sect1 id="synaptic">synaptic

<p><prgn/synaptic/ is a graphical package manager.  It enables you to install,
upgrade and remove software packages in a user friendly way.  Along with most
of the features offered by aptitude, it also has a feature for editing the list of
used repositories, and supports browsing all available documentation related to
a package. See the <url id="http://www.nongnu.org/synaptic/" name="Synaptic
Website"> for more information.


<!-- once these are mature, add a section on them:
      adept-manager (for KDE)
      update-manager (for GNOME)   -->


<sect1 id="tasksel">tasksel

<p>When you want to perform a specific task it might be difficult to
find the appropiate suite of packages that fill your need. 
The Debian developers have defined <tt/tasks/, a task is a collection of
several individual Debian packages all related to a specific activity.
Tasks can be installed through the <prgn/tasksel/ program or through
<prgn/aptitude/.

<p>Typically, the Debian installer will automatically install the task
associated with a standard system and a desktop environment. The specific
desktop environment installed will depend on the CD/DVD media used, most
commonly it will be the GNOME desktop (<tt/gnome-desktop/ task).  Also,
depending on your selections throughout the installation process, tasks might
be automatically installed in your system. For example, if you selected a
language other than English, the task associated with it will be installed automatically too and
if the installer recognises you are installing on a laptop system the <tt/laptop/
task will also be installed.

<sect1 id="dpkg-extra">Other package management tools

<sect2 id="dpkg-deb">dpkg-deb

<p>This program manipulates Debian archive (<tt>.deb</tt>) files.
Some common uses are:
<list>
  <item>Find out all the options:  <tt>dpkg-deb --help</tt>.
  <item>Determine what files are contained in a Debian
    archive file:  <tt>dpkg-deb --contents foo_VVV-RRR.deb</tt>)
  <item>Extract the files contained in a named Debian archive into a
    user specified directory:  <tt>dpkg-deb --extract foo_VVV-RRR.deb tmp</tt>
    extracts each of the files in <tt>foo_VVV-RRR.deb</tt> into the
    directory <tt>tmp/</tt>.  This is convenient for examining the contents
    of a package in a localized directory, without installing the package
    into the root file system.
  <item>Extract the control information files from a package:
    <tt>dpkg-deb --control foo_VVV-RRR.deb tmp</tt>.
</list>

<p>Note that any packages that were merely unpacked using <tt/dpkg-deb
--extract/ will be incorrectly installed, you should use <tt/dpkg --install/
instead.

<p>More information is given in the manual page <manref name="dpkg-deb"
section="1">.

</sect1>

<sect id="updaterunning">Debian claims to be able to update a running program;
  how is this accomplished?

<p>The kernel (file system) in &debian; systems supports replacing files even
while they're being used.

<p>We also provide a program called <prgn/start-stop-daemon/ which is used
to start daemons at boot time or to stop daemons when the runlevel is
changed (e.g., from multi-user to single-user or to halt). The same program
is used by installation scripts when a new package containing a daemon is
installed, to stop running daemons, and restart them as necessary.

<sect id="whatpackages">How can I tell what packages are already installed
  on a Debian system?

<p>To learn the status of all the packages installed on a Debian system,
execute the command
  <example>dpkg --list</example>

This prints out a one-line summary for each package, giving a 2-letter
status symbol (explained in the header), the package name, the version
which is <em>installed</em>, and a brief description.

<p>To learn the status of packages whose names match any
pattern beginning with "foo", run the command:
  <example>dpkg --list 'foo*'</example>

<p>To get a more verbose report for a particular package, execute the
command:
  <example>dpkg --status packagename</example>
 
<sect id="listfiles">How do I display the files of an installed package?

<p>To list all the files provided by the installed package <tt>foo</tt>
execute the command
	<example>dpkg --listfiles foo</example>
	
<p>Note that the files created by the installation scripts aren't displayed.

<sect id="filesearch">How can I find out what package produced a particular
  file?

<p>To identify the package that produced the file named <tt>foo</tt> execute
either:

<list>
  <item><tt>dpkg --search foo</tt>
    <p>This searches for <tt>foo</tt> in installed packages.
    (This is (currently) equivalent to searching all of the files having the
    file extension of <tt>.list</tt> in the directory
    <tt>/var/lib/dpkg/info/</tt>, and adjusting the output to print the names
    of all the packages containing it, and diversions.)

    <p>A faster alternative to this is the <prgn>dlocate</prgn> tool.
	<example>dlocate -S foo</example>

  <item><tt>zgrep foo Contents-ARCH.gz</tt>
    <p>This searches for files which contain the substring <tt>foo</tt>
    in their full path names.  The files <tt>Contents-ARCH.gz</tt> (where ARCH
    represents the wanted architecture) reside in the major package directories
    (main, non-free, contrib) at a Debian FTP site (i.e. under 
    <tt>/debian/dists/&releasename;</tt>).  A <tt>Contents</tt> file
    refers only to the packages in the subdirectory tree where it resides.
    Therefore, a user might have to search more than one <tt>Contents</tt>
    files to find the package containing the file <tt>foo</tt>.

    <p>This method has the advantage over <tt>dpkg --search</tt> in that it
    will find files in packages that are not currently installed on your
    system.

  <item><tt>apt-file search <var>foo</var></tt>
    <p>If you install the <package>apt-file</package> package, similar to the above, it
    searches files which contain the substring or regular expression
    <tt>foo</tt> in their full path names. The
    advantage over the example above is that there is no need to retrieve the
    <tt>Contents-ARCH.gz</tt> files as it will do this automatically for
    all the sources defined in <file>/etc/apt/sources.list</file> when
    you run (as root) <tt>apt-file update</tt>.
</list>

<sect id="datapackages">Why is `foo-data' not removed when I 
   uninstall `foo'?  How do I make sure old unused library-packages get
   purged?

<p>Some packages are split in program (`foo') and data
(`foo-data') (or in `foo' and `foo-doc'). This is true for many games,
multimedia applications and
dictionaries in Debian and has been introduced since some users might
want to access the raw data without installing the program or because
the program can be run without the data itself, making `foo-data' optional.

<p>Similar situations occur when dealing with libraries: generally these get
installed since packages containing applications depend on them.  When the
application-package is purged, the library-package might stay on the system.
Or: when the application-package no longer depends upon e.g. libdb4.2, but upon
libdb4.3, the libdb4.2 package might stay when the application-package is
upgraded.

<p>In these cases, `foo-data' doesn't depend on `foo', so when you
remove the `foo' package it will not get automatically removed by most
package management tools. The same holds true for
the library packages. This is necessary to avoid circular
dependencies. However, if you use <prgn>apt-get</prgn> 
(see <ref id="apt-get">) or <prgn>aptitude</prgn> (see <ref id="aptitude">)
as your package management tool, they will
track automatically installed packages and give the possibility to
remove them, when no packages making use of them remain in your system.

